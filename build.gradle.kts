import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version "1.4.31"
  application
  id("io.gitlab.arturbosch.detekt") version "1.16.0"
  id("org.jlleitschuh.gradle.ktlint") version "10.0.0"
}

group = "me.viddi"
version = "1.0"

repositories {
  mavenCentral()
  jcenter()
  gradlePluginPortal()
}

dependencies {
  implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.3")
  implementation("com.squareup.okhttp3:okhttp:4.9.1")

  testImplementation(kotlin("test-junit5"))
  testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
  testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")
  testImplementation("com.squareup.okhttp3:mockwebserver:4.9.1")
}

tasks.test {
  useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
  kotlinOptions.jvmTarget = "11"
}

tasks.withType<Jar> {
  manifest {
    attributes["Main-Class"] = "pixels.main"
  }
}

application {
  mainClassName = "MainKt"
}

detekt {
  allRules = true

  reports {
    sarif.enabled = false
    xml.enabled = false
  }
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
  jvmTarget = "1.8"
}
