# Pixels

Using Kotlin's [Coroutine's](https://kotlinlang.org/docs/coroutines-overview.html) with [Chanels](https://kotlinlang.org/docs/channels.html).

## Structure

Based on producer-consumer pattern.

### Workers

The problem is broken out into three different workers: `requestImage`, `processImage` and `countHexValues`.
Each stage has its own thread pool with various amounts of workers (see constants at the top of main.kt). The number of workers matters because 
each stage in the pipeline takes different amount of time. For example, processing the images takes longer
than downloading the image, so over time there will be backpressure from `requestImage` to `processImage`.

The values for how many workers are spawned can be tweaked.

#### Communication

Workers communicate via kotlin Channels, which are basically BlockingQueue's. There is a Channel defined for each
entry point into each stage. A worker will do a blocking wait for a new message and filter that based on what
it's expecting. Hence there are 4 Channels in total: `requestChannel`, `processChannel`, `countChannel` and `completedChannel`.

### Pipeline

The input file is ingested as a stream, which will send urls to the first channel `requestChannel`. Once
an image has been downloaded, a message will be sent to the `processChannel`, then to `countChannel` and finally
to the `completedChannel`. The last channel is being waited on in main.kt on the main thread.

## Commands

Most convenient way is to use the gradle wrapper.

Run:

```
./gradlew run
```

## TODOs

If I were to spend more time on this, I'd create a Docker image so this can be executed more easily
and change both the number of workers to be environment variables, and expect the input file to
be mounted to the container. This would make the program more flexible once it's been packaged.

In addition I'd obviously add more tests, with single color test images and feed that into the pipeline.
