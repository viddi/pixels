import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Buffer
import okio.source
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestRequestImage {
  companion object {
    val server = MockWebServer()
    val baseUrl = server.url("/")
  }

  @Test
  fun testRequestImage() = runBlocking {
    server.enqueue(
      MockResponse()
        .setResponseCode(200)
        .setBody(Buffer().apply { writeAll(TestUtil.getTestImage().source()) })
    )

    val countDownLatch = CountDownLatch(1)
    val sendChannel = Channel<Message>()
    val receiveChannel = Channel<Message>()

    requestImage(0, sendChannel, receiveChannel)

    launch(Dispatchers.Default) {
      for (msg in receiveChannel) {
        if (msg is ProcessImage) {
          assertEquals(baseUrl.toString(), msg.url)
          assertTrue(
            TestUtil.inputStreamEquals(
              TestUtil.getTestImage().inputStream(),
              msg.response.body!!.byteStream()
            )
          )

          countDownLatch.countDown()
          sendChannel.close()
          receiveChannel.close()
        }
      }
    }

    val message = RequestImage(baseUrl.toString())
    sendChannel.send(message)
    assertTrue(countDownLatch.await(1, TimeUnit.SECONDS))
  }
}
