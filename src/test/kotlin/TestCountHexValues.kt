import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestCountHexValues {

  @Test
  fun testHexCount() = runBlocking {
    val values = mapOf(
      "#FFFFFFFF" to 20,
      "#FFC6BBBF" to 9,
      "#FF444A5A" to 5,
      "#FFF3C300" to 50,
      "#FF929F7C" to 11
    )

    val countDownLatch = CountDownLatch(1)
    val sendChannel = Channel<Message>()
    val receiveChannel = Channel<Message>()

    countHexValues(0, sendChannel, receiveChannel)

    launch(Dispatchers.Default) {
      for (msg in receiveChannel) {
        when (msg) {
          is Completed -> {
            val expected = "test-url,#FFF3C300,#FFFFFFFF,#FF929F7C"
            assertEquals(expected, msg.csv)

            countDownLatch.countDown()
            sendChannel.close()
            receiveChannel.close()
          }
        }
      }
    }

    val message = CountHexValues(values, "test-url")
    sendChannel.send(message)
    assertTrue(countDownLatch.await(5, TimeUnit.SECONDS))
  }
}
