import java.io.DataInputStream
import java.io.EOFException
import java.io.File
import java.io.InputStream

object TestUtil {

  fun getTestImage(): File {
    return File("src/test/resources/test-image.jpg")
  }

  fun inputStreamEquals(i1: InputStream, i2: InputStream): Boolean {
    val buf1 = ByteArray(64 * 1024)
    val buf2 = ByteArray(64 * 1024)

    return try {
      val d2 = DataInputStream(i2)
      var len: Int

      while (i1.read(buf1).also { len = it } > 0) {
        d2.readFully(buf2, 0, len)
        for (i in 0 until len) if (buf1[i] != buf2[i]) return false
      }

      d2.read() < 0
    } catch (ioe: EOFException) {
      false
    } finally {
      i1.close()
      i2.close()
    }
  }
}
