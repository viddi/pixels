import okhttp3.Response

sealed class Message

data class RequestImage(val url: String) : Message()

data class ProcessImage(val response: Response, val url: String) : Message()

data class CountHexValues(val hexValues: Map<String, Int>, val url: String) : Message()

data class Completed(val csv: String) : Message()

object Error : Message()
