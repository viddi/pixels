import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.launch
import java.util.concurrent.Executors

val storeContext = Executors.newFixedThreadPool(COUNT_HEX_VALUES_WORKERS).asCoroutineDispatcher()

fun CoroutineScope.countHexValues(worker: Int, inbound: ReceiveChannel<Message>, outbound: SendChannel<Message>) =
  launch(storeContext) {
    for (msg in inbound) {
      when (msg) {
        is CountHexValues -> {
          println("CountHexValues worker: $worker: counting values for ${msg.url}")

          var first = HexCount("", 0)
          var second = HexCount("", 0)
          var third = HexCount("", 0)

          msg.hexValues.forEach { (hex, count) ->
            when {
              count >= first.count -> {
                third = second
                second = first
                first = HexCount(hex, count)
              }
              count >= second.count -> {
                third = second
                second = HexCount(hex, count)
              }
              count >= third.count -> {
                third = HexCount(hex, count)
              }
            }
          }

          println("CountHexValues worker: $worker: finished counting values for ${msg.url}")
          val csv = "${msg.url},${first.hex},${second.hex},${third.hex}"
          outbound.send(Completed(csv))
        }
        is Error -> outbound.send(msg)
      }
    }
  }

private data class HexCount(val hex: String, val count: Int)
