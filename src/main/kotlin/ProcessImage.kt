import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.launch
import java.awt.image.BufferedImage
import java.awt.image.DataBufferByte
import java.io.IOException
import java.io.InputStream
import java.util.Locale
import java.util.concurrent.Executors
import javax.imageio.ImageIO

val processContext = Executors.newFixedThreadPool(PROCESS_IMAGE_WORKERS).asCoroutineDispatcher()

fun CoroutineScope.processImage(worker: Int, inbound: ReceiveChannel<Message>, outbound: SendChannel<Message>) =
  launch(processContext) {
    for (msg in inbound) {
      when (msg) {
        is ProcessImage -> {
          msg.response.body?.byteStream()?.let { inputStream ->
            println("ProcessImage worker $worker: processing image from ${msg.url}")
            inputStream.toImage()?.getHexCountForImage()?.let { values ->
              println("ProcessImage worker $worker: done processing image from ${msg.url}")
              msg.response.close()
              outbound.send(CountHexValues(values, msg.url))
            } ?: kotlin.run {
              println("ProcessImage worker $worker: unable to process image from ${msg.url}")
              outbound.send(Error)
            }
          } ?: run {
            println("ProcessImage worker $worker: No image in response from ${msg.url}")
            msg.response.close()
            outbound.send(Error)
          }
        }
        is Error -> outbound.send(msg)
      }
    }
  }

@Suppress("MagicNumber")
private fun BufferedImage.getHexCountForImage(): Map<String, Int> {
  val pixels = (raster.dataBuffer as DataBufferByte).data
  val hasAlphaChannel = alphaRaster != null

  val values = mutableMapOf<String, Int>()

  if (hasAlphaChannel) {
    val pixelLength = 4
    var pixel = 0

    while (pixel + 3 < pixels.size) {
      var argb = 0
      argb += pixels[pixel].toInt() and 0xff shl 24 // alpha
      argb += pixels[pixel + 1].toInt() and 0xff // blue
      argb += pixels[pixel + 2].toInt() and 0xff shl 8 // green
      argb += pixels[pixel + 3].toInt() and 0xff shl 16 // red

      val hex = String.format(Locale.US, "#%02X", argb)
      values[hex]?.let { count -> values[hex] = count + 1 } ?: run { values[hex] = 1 }

      pixel += pixelLength
    }
  } else {
    val pixelLength = 3
    var pixel = 0

    while (pixel + 2 < pixels.size) {
      var argb = 0
      argb += -16777216 // 255 alpha
      argb += pixels[pixel].toInt() and 0xff // blue
      argb += pixels[pixel + 1].toInt() and 0xff shl 8 // green
      argb += pixels[pixel + 2].toInt() and 0xff shl 16 // red

      val hex = String.format(Locale.US, "#%02X", argb)
      values[hex]?.let { count -> values[hex] = count + 1 } ?: run { values[hex] = 1 }

      pixel += pixelLength
    }
  }

  return values
}

@Suppress("SwallowedException")
private fun InputStream.toImage(): BufferedImage? {
  return try {
    ImageIO.read(this)
  } catch (e: IOException) {
    null
  }
}
