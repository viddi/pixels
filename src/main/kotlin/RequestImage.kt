import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.Executors

val requestContext = Executors.newFixedThreadPool(REQUEST_IMAGE_WORKERS).asCoroutineDispatcher()

private val httpClient = OkHttpClient()

fun CoroutineScope.requestImage(worker: Int, inbound: ReceiveChannel<Message>, outbound: SendChannel<Message>) =
  launch(requestContext) {
    for (msg in inbound) {
      when (msg) {
        is RequestImage -> {
          println("RequestImage worker $worker: downloading image from ${msg.url}")
          msg.url.getImage()?.let { response ->
            outbound.send(ProcessImage(response, msg.url))
          } ?: kotlin.run {
            println("RequestImage worker $worker: failed to download image from ${msg.url}")
            outbound.send(Error)
          }
        }
      }
    }
  }

@Suppress("SwallowedException")
private fun String.getImage(): Response? {
  return try {
    val request = Request.Builder()
      .url(this)
      .build()

    httpClient.newCall(request)
      .execute()
  } catch (e: IOException) {
    null
  }
}
