import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.streams.asSequence
import kotlin.system.measureTimeMillis

const val INPUT_FILE_PATH = "input.txt"
const val OUTPUT_FILE_PATH = "output.csv"
const val REQUEST_IMAGE_WORKERS = 2
const val PROCESS_IMAGE_WORKERS = 40
const val COUNT_HEX_VALUES_WORKERS = 5
const val PROCESS_CHANNEL_BUFFER = 100
const val COUNT_CHANNEL_BUFFER = 100

const val TAKE_TOTAL = 1000

fun main() = runBlocking {
  val inputResource = object {}.javaClass.getResource(INPUT_FILE_PATH)

  val csvWriter = Files.newBufferedWriter(Paths.get(OUTPUT_FILE_PATH))
  // Set header for csv
  csvWriter.append("url,first,second,third")
  csvWriter.newLine()

  val requestChannel = Channel<Message>(Channel.UNLIMITED)
  val processChannel = Channel<Message>(Channel.UNLIMITED)
  val countChannel = Channel<Message>(Channel.UNLIMITED)
  val completedChannel = Channel<Message>(Channel.UNLIMITED)

  repeat(REQUEST_IMAGE_WORKERS) { worker ->
    requestImage(worker, requestChannel, processChannel)
  }

  repeat(PROCESS_IMAGE_WORKERS) { worker ->
    processImage(worker, processChannel, countChannel)
  }

  repeat(COUNT_HEX_VALUES_WORKERS) { worker ->
    countHexValues(worker, countChannel, completedChannel)
  }

  var urlCount = 0
  var finalCount: Int
  val time = measureTimeMillis {
    Files.lines(Paths.get(inputResource.path))
      .asSequence()
//      .take(TAKE_TOTAL)
      .forEach { url ->
        urlCount++
        requestChannel.send(RequestImage(url))
      }
      .also { finalCount = urlCount }

    var doneCount = 0
    for (msg in completedChannel) {
      when (msg) {
        is Completed -> {
          csvWriter.append(msg.csv)
          csvWriter.newLine()
          csvWriter.flush()

          println("Write line to output: ${msg.csv}")

          doneCount++
          if (doneCount == finalCount) {
            break
          }
        }
        is Error -> doneCount++
      }
    }
  }

  println("Processing images took $time ms")
  csvWriter.close()
  requestContext.close()
  processContext.close()
  storeContext.close()
  coroutineContext.cancelChildren()
}
